#!/usr/bin/env python3
import os
import sys

# Automatically detect if their is an .env_path file to enable the
# virtual env environment
PROJECT_DIR = os.path.realpath(os.path.dirname(__file__))
ENV_PATH = os.path.join(PROJECT_DIR, '.env_path')

if os.path.exists(ENV_PATH) and os.access(ENV_PATH, os.R_OK):
    with open(ENV_PATH, 'r') as fi:
        _env_path = fi.read().strip()

        if os.path.exists(_env_path):
            _activate_this = os.path.join(_env_path, 'bin/activate_this.py')
            exec(open(_activate_this).read(), dict(__file__=_activate_this))
    sys.path.append(PROJECT_DIR)

if __name__ == '__main__':
    os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'cmt.settings')
    try:
        from django.core.management import execute_from_command_line
    except ImportError as exc:
        raise ImportError(
            "Couldn't import Django. Are you sure it's installed and "
            "available on your PYTHONPATH environment variable? Did you "
            "forget to activate a virtual environment?"
        ) from exc
    execute_from_command_line(sys.argv)
